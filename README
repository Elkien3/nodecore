========================================================================

             \                           /\
              \                         /  \
          /\   \     /\    |\      /   /    \    /\    |\     /
         |  \   |   /  \   | \    /   |         /  \   | \   /
         |   \  |  |    |  |  |  |__  |        |    |  | /  |__
          \   \/   |    |  |  |  |     \    /  |    |  |/   |
           \        \  /   | /    \     \  /    \  /   |\    \
            \        \/    |/      \     \/      \/    | \    \

========================================================================
  NodeCore - An original, immersive puzzle/adventure game for Minetest
------------------------------------------------------------------------

NodeCore is a major reconstruction of survival gameplay from the ground
up.  Relearn much of what you thought you knew about Minetest!

Features:

- Immersive gameplay.  Zero in-game pop-up GUIs.  Minimal HUDs.
  All interactions take place in continuous first-person perspective.
  Nothing gets between you and the game world.

- Nearly everything is on the grid. Item stacks settle onto the grid
  and persist.  Complex machines (e.g. furnaces, storage) are built
  in-world and can be designed, customized, optimized.

- In-world crafting.  Assemble more complex recipes directly on the grid
  or in-place.  Learn how to "pummel" nodes to hammer things together
  or break things apart.

- Built-in player guide and hint system provides low-spoiler guidance.

- Rich and subtle interactions.  Some materials dig and transform in
  stages. Some materials slump into repose when stacked high.  Some
  things transform if conditions are met, but transform faster based
  on environment.  Experiment and see what effects what!

- Build complex in-game systems.  Design your own furnace.  Construct
  digital logic circuits.  Build gears and rotary machinery.  Assemble
  portable storage containers.

- Eggcorns!

------------------------------------------------------------------------

NodeCore is (C)2018-2019 by Aaron Suen <warr1024@gmail.com>
MIT License (http://www.opensource.org/licenses/MIT)
See included LICENSE file for full details and credits

Minetest ContentDB Listing:
	https://content.minetest.net/packages/Warr1024/nodecore/

GitLab:
	https://gitlab.com/sztest/nodecore

Discord:
	https://discord.gg/SHq2tkb

IRC:
	#nodecore on chat.freenode.net
	(bridged to Discord)

------------------------------------------------------------------------
========================================================================
