-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("ore")
include("fluid")
include("react")
include("tools")
include("cherenkov")
include("radiation")
